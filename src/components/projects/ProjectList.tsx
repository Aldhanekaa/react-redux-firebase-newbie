import React from 'react';
import ProjectSummary from './ProjectSummary';

interface ComponentInterface {
  (): JSX.Element;
}

const ProjectList: ComponentInterface = () => {
  return (
    <div className='project-list section'>
      <ProjectSummary />
      <ProjectSummary />
      <ProjectSummary />
      <ProjectSummary />
    </div>
  );
};

export default ProjectList;
