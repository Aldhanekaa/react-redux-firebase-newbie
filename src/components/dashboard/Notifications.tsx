import React from 'react';

interface NotificationsInterface {
  (): JSX.Element;
}

const Notifications: NotificationsInterface = () => {
  return (
    <div>
      <p>Notifications</p>
    </div>
  );
};

export default Notifications;
