import React, { Component, ReactPropTypes } from 'react';
import ProjectList from '../projects/ProjectList';
import Notifications from './Notifications';
import { connect, MapStateToProps, MapStateToPropsParam } from 'react-redux';

import RootSate, { projectState, authState } from '../../store/types/';

const mapStateToProps = (state: RootSate) => {
  return {
    projects: state.project.projects,
  };
};
type ReduxType = ReturnType<typeof mapStateToProps>;

class Dashboard extends Component<ReduxType> {
  componentDidMount = () => {
    console.log(this.props.projects);
  };
  render() {
    return (
      <div className='dashboard container'>
        <div className='row'>
          <div className='col s12 m6'>
            <ProjectList />
          </div>
          <div className='col s12 m5 offset-m1'>
            <Notifications />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Dashboard);
