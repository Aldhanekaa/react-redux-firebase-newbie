export enum projectActions {
  ADD_ITEM,
}

export enum authActions {
  LOGIN,
  SIGNUP,
}
