export interface projectState {
  projects: object[];
}

export interface authState {
  auth: object;
}
