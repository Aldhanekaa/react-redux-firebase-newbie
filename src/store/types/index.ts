import { projectState, authState } from './states';
import { projectActions, authActions } from './actions';

interface RootState {
  project: projectState;
  action: authState;
}

export { projectActions, authActions };
export type { projectState, authState };
export default RootState;
