import { projectState } from '../types/states';
import { projectActions } from '../types/actions';

const initState: projectState = {
  projects: [
    { id: '1', title: 'help me find peach', content: 'blah blah blah' },
    { id: '2', title: 'collect all the stars', content: 'blah blah blah' },
    { id: '3', title: 'egg hunt with yoshi', content: 'blah blah blah' },
  ],
};

interface projectReducerInterface {
  (state: projectState, action: projectActions): projectState;
}

const projectReducer: projectReducerInterface = (
  state: projectState = initState,
  action: projectActions,
) => {
  return state;
};

export default projectReducer;
