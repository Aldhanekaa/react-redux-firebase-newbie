import { authActions } from '../types/actions';
import { authState } from '../types/states';

const initState: authState = {
  auth: {
    login: true,
  },
};

const authReducer = (state: object = initState, action: authActions) => {
  return state;
};

export default authReducer;
